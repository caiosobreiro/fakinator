import readline from 'readline-promise';

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: true
});

export function askQuestion (text) {
    return rl.questionAsync(`>>> ${text}\n=> `);
}
