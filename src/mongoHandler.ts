import * as mongoose from "mongoose";

class MongoHandler {

    ObjectId = mongoose.Types.ObjectId;

    targetModel = this.createModel('target', { label: String });
    questionModel = this.createModel('question', { question: String, yes: [this.ObjectId], no: [this.ObjectId] });

    constructor () {
        const host = 'localhost:27017';
        const dbName = 'fakinator';
        try {
            mongoose.connect(`mongodb://${host}/${dbName}`, {useNewUrlParser: true, useUnifiedTopology: true});
        } catch (err) {
            console.log(`Couldn't connect to db\nIs mongo running?`);
            process.exit(1);
        }
    }

    createModel (name: string, schema) {
        return mongoose.model(name, schema);
    }

}

export const mongoHandler = new MongoHandler();
