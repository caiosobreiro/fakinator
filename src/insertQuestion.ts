import { mongoHandler } from "./mongoHandler";
import { askQuestion } from "./outputHandler";

async function main(){
    const question = await askQuestion('Insira uma pergunta:');
    let targets = await mongoHandler.targetModel.find().lean().exec();

    const yesArr = [];
    const noArr = [];
    for (let target of targets) {
        const answer = await askQuestion(target.label);
        if (answer === 'y' || answer === 's') {
            yesArr.push(target._id);
        } else {
            noArr.push(target._id);
        }
    };

    const newQuestion = new mongoHandler.questionModel({ question, yes: yesArr, no: noArr });
    newQuestion.save();
    return;
}

main().then().catch();
