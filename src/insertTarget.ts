import { mongoHandler } from "./mongoHandler";
const objList = require('../objetos.json');

async function main(){
    objList.forEach(obj => {
        const label = obj;
        let targetModel = new mongoHandler.targetModel({ label });
        targetModel.save();
    });
    return;
}

main().then().catch();
