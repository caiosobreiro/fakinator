import * as _ from 'lodash';
import { mongoHandler } from './mongoHandler';
import { askQuestion } from './outputHandler';

async function main() {

    let targets = await mongoHandler.targetModel.find().lean().exec();
    let questions = await mongoHandler.questionModel.find().lean().exec();

    targets = targets.map(target => {
        target._id = target._id.toString();
        return target;
    });

    questions = questions.map(question => {
        question.yes = question.yes.map(id => id.toString());
        question.no = question.no.map(id => id.toString());
        return question;
    });

    const toAsk = [getBestQuestion(targets, questions)];

    for (const question of toAsk) {
        const answer = await askQuestion(question.question);
        const method = answer === 'y' || answer === 's' ? 'yes' : 'no';
        targets = targets.filter(target => question[method].includes(target._id));
        const nextQuestion = getBestQuestion(targets, questions);
        if (targets.length > 1 && nextQuestion !== undefined) toAsk.push(nextQuestion);
    }

    // 100% de certeza
    if (targets.length === 1) console.log('Você escolheu:', targets[0].label);
    // Nenhum item encontrado
    else if (!targets.length) console.log('Nenhum item com essas características encontrado. Tente novamente');
    //Impossível filtrar mais, escolha um aleatoriamente
    else console.log('\nEu acho que você pensou em', _.sample(targets).label, '\n\nDúvidas:', targets.map(target => target.label).join(', '));
    return;
}

function getBestQuestion (targets, questions) {
    const worstScore = targets.length / 2;
    const targetIds = targets.map(target => target._id);
    let bestQuestion;
    let bestScore = worstScore;
    _.forEach(questions, question => {
        const intersectionArrSize = _.intersection(targetIds, question.yes).length;
        const score = Math.abs(worstScore - intersectionArrSize);
        if (score < bestScore) {
            bestQuestion = question;
            bestScore = score;
            if (score === 0) return bestQuestion;
        }
    });
    return bestQuestion;
}

main().then().catch();
